Ce dépôt contient les fichiers de sous-titres de la saison 11 de Doctor Who (2005) traduits par La Team TRAD'IS, via Addic7ed.


## Progression
|     | HDTV | 720p.HDTV | 1080p.HDTV | iP.WEB-DL | AMZN.WEB-DL | Blu-ray |
|-----|------|-----------|------------|-----------|-------------|---------|
| E01 | ✓    | ✓         | ✓          | ✓         | ✓           |  ✓      |
| E02 | ✓    | ✓         | ✓          | ✓         | ✓           |  ✓      |
| E03 | ✓    | ✓         | ✓          | ✓         | ✓           |  ✓      |
| E04 | ✓    | ✓         | ✓          | ✓         | ✓           |  ✓      |
| E05 | ✓    | ✓         | ✓          | ✓         | ✓           |  ✓      |
| E06 | ✓    | ✓         | ✓          | ✓         | ✓           |  ✓      |
| E07 | ✓    | ✓         | ✓          | ✓         | ✓           |  ✓      |
| E08 | ✓    | ✓         | ✓          | ✓         | ✓           |  ✓      |
| E09 | ✓    | ✓         | ✓          | ✓         | ✓           |  ✓      |
| E10 | ✓    | ✓         | ✓          | ✓         | ✓           |  ✓      |

## Compatibilité
E01 : FoV = iP.WEB-DL / MTB = MTB

E02 : 720pMTB = iP.WEB-DL

E03 : FoV = iP.WEB-DL / MTB = MTB

E04 : FoV = iP.WEB-DL = RiVER

E05 : Ø

E06 : FoV = MTB = MTB / FAiLED = iP.WEB-DL

E07 : FoV = KETTLE = KETTLE

E08 : KETTLE = BTW

E09 : KETTLE = KETTLE / iP.WEB-DL = FAiLED

E10 : KETTLE = KETTLE
